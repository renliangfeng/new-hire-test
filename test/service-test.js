describe('Staff Search Service', function() {

	var staffService;

	beforeEach(module('mq.service'));

	describe('parseQuery', function() {
		beforeEach(inject(function(_StaffService_) {
			staffService = _StaffService_;
		}));

		it('empty', function() {
    		var object = staffService.parseQuery("");
        	expect(object.name).toBeUndefined();
        	expect(object.role).toBeUndefined();
        	expect(object.office).toBeUndefined();
    	});

    	it('blank', function() {
    		var object = staffService.parseQuery("   ");
        	expect(object.name).toBeUndefined();
        	expect(object.role).toBeUndefined();
        	expect(object.office).toBeUndefined();
    	});

		it('filter by name', function() {
    		var object = staffService.parseQuery("name:alex");
        	expect(object.name).toBe("alex");
        	expect(object.role).toBeUndefined();
        	expect(object.office).toBeUndefined();
    	});

    	it('filter by role', function() {
    		var object = staffService.parseQuery("role:dev");
    		expect(object.name).toBeUndefined();
        	expect(object.role).toBe("dev");        	
        	expect(object.office).toBeUndefined();
    	});

    	it('filter by office', function() {
    		var object = staffService.parseQuery("office:Syd");
    		expect(object.name).toBeUndefined();        
    		expect(object.role).toBeUndefined();        	
        	expect(object.office).toBe("Syd");
    	});

    	it('filter by name and role', function() {
    		var object = staffService.parseQuery("name:alex  role:driver");
        	expect(object.name).toBe("alex");
        	expect(object.role).toBe("driver");
        	expect(object.office).toBeUndefined();
    	});

    	it('filter by role and office', function() {
    		var object = staffService.parseQuery("office:mel role:driver");
        	expect(object.name).toBeUndefined();
        	expect(object.role).toBe("driver");
        	expect(object.office).toBe("mel");
    	});

    	it('filter by name, role and office', function() {
    		var object = staffService.parseQuery("name:stev office:Wogga role:Tea");
        	expect(object.name).toBe("stev");;
        	expect(object.role).toBe("Tea");
        	expect(object.office).toBe("Wogga");
    	});

    	it('filter by role but with blank name and office', function() {
    		var object = staffService.parseQuery("name:  role:Lawer office:  ");
        	expect(object.name).toBeUndefined();
        	expect(object.role).toBe("Lawer");
        	expect(object.office).toBeUndefined();
    	});

    	it('blank name, blank role and office with value', function() {
    		var object = staffService.parseQuery("name:  role: office:Sydney ");
        	expect(object.name).toBeUndefined();
        	expect(object.role).toBeUndefined();
        	expect(object.office).toBe("Sydney");
    	});

	});	

   

   
});
