'use strict';

// Declare app level module which depends on views, and components
angular.module('mq', [
	'mq.service'
])
.controller('StaffSearchController', 
    ['$scope', 'StaffService', function ($scope, StaffService) {    	
		$scope.staffs = StaffService.getAllStaffs();
		$scope.query = "";
		
		$scope.filterStaff = function () {
			return StaffService.parseQuery($scope.query);
		};

    }]
);


