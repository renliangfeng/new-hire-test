'use strict';

angular.module('mq.service', [])

.factory('StaffService', function () {
  var service = {};

  service.getAllStaffs = function () {
  	var staffs = [
			{
				name: 'Alex',
				role: 'Developer',
				office: 'Sydney'
			},
			{
				name: 'Ben',
				role: 'Developer',
				office: 'Wogga'
			},
			{
				name: 'Sam',
				role: 'Teacher',
				office: 'Sydney'
			},
			{
				name: 'Steve',
				role: 'Builder',
				office: 'Melbourne'
			},
			{
				name: 'Josh',
				role: 'Driver',
				office: 'Sydney'
			},
			{
				name: 'Sarah',
				role: 'Lawyer',
				office: 'Brisbane'
			}
		];
	return staffs;

  };

  //parse the query string and return object used by filter filter to filter the staff list
  service.parseQuery = function (queryStr) {
		var filterObject = {};
		var props = queryStr.split(" ");
		props.forEach(function (prop) {
			var index = prop.indexOf(":");
			if (index > 0) {
				var propName = prop.substring(0, index).trim();
				var propValue = prop.substring(index+1).trim();
				if (propName != "" && propValue != "") {
					filterObject[propName] = propValue;
				}
			}
		});
		return filterObject;
	};
  
  return service; 
  
})

